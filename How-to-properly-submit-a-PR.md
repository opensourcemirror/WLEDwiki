# Managing a Forked Branch

If you plan to submit a PR bigger than a simple change in one file, here is a short intro about how to do a clean PR.

* Fork the WLED repository in Github (by clicking the "Fork" button in the upper-right corner of GitHub)
* Clone your repository locally via `git clone https://github.com/<github_user>/WLED.git` (replacing `<github_user>` with your GitHub Username) and work on your local copy
* Add the upstream repository via `git remote add upstream https://github.com/Aircoookie/WLED.git`
* Ensure you are on the `main` branch via `git checkout main` (alternatively you may want to check out a *specific* branch if instructed so, usually it will be a branch for the next WLED version or any other active branch)
* Create and change to a new branch via `git checkout -b <temp_branch>` (replacing `<temp_branch>` with the name you want to use for your temporary branch) to create a working branch where you can push commits
* Set the upstream branch via `git push --set-upstream origin <temp_branch>` (replacing `<temp_branch>` with the name you used in the previous step)
* Work on your local version and push as many commits as you want

When you think it is ready to merge and submit a PR:

* Return to the `main` branch via `git checkout main` (or the *specific* branch you selected 1st). This will not erase any committed changes you have made.
* Ensure your `main` branch is up-to-date with the latest changes via `git pull upstream main` (alternatively replace `main` with *specific* branch)
* Update your fork of WLED by running `git push`
* Create a new branch to be used for your PR via `git checkout -b <pr_branch>` (replacing `<pr_branch>` with a new branch name, which must be different from `<temp_branch>`)
* Set the upstream branch via `git push --set-upstream origin <pr_branch>` (replacing `<pr_branch>` with the name you used in the previous step)
* Merge the edits but be sure to remove the history of your local commits via `git merge --squash <temp_branch>` (replacing `<temp_branch>` with the branch name you used earlier)
* Create a new commit containing all of your merged changes via `git commit -m "<Message>"` (replacing `<Message>` with information regarding the contents of your changes)

Now you have a clean single commit from which you can create the PR on the WLED Github.

## Alternative

GitHub provides a drop-down option to apply a PR as a "Squash and Merge", directly from the PR. GitHub even supports [configuring the depot](https://github.com/dotnet/runtime/issues/260#issuecomment-649622840) such that PRs *must* be "Squash and Merge". See picture at end of page.

However, if you're requested to manually squash and merge **before** the PR is reviewed and approved, you can create a second branch on GitHub and perform the squash and merge entirely via the website:

_Note: for legacy depots, instead of the branch name 'main', you may see a branch name of 'master'._

<details><summary>1. Ensure your fork's main branch is up-to-date with the upstream depot</summary><P/>

* goto your fork of the depot on github.com
* ensure you are viewing the branch 'main'
* select the "fetch upstream" button, then "fetch and merge"

</details>
<details><summary>2. Create a new branch based off your main branch</summary><P/>

* goto your fork of the depot on github.com
* ensure you are viewing the branch 'main'
* click on the branch name
* Type a name for your new branch; I'll use the branch name `foo` in this example
* click on the `create branch: foo from 'main'` that appears after you type the name

</details>
<details><summary>3. Create a PR to this new branch</summary><P/>

* goto your fork of the depot on github.com
* select the `pull requests` tab (on your fork ... not the upstream depot)
* select `New pull request` button
* the `base repository` will default to the upstream depot; change it to your fork
* _**note the UI is now much simpler :smiley:**_
* for the `base` branch, select the newly created branch (e.g., `foo`)
* for the `compare` branch, select the old PR branch (e.g., `oldPR`)
* click the resulting `Create pull request` button
* Give the PR a title and a comment
* Click to "submit" the PR to yourself...

</details>
<details><summary>4. Approve your own PR, but select the option "Squash and Merge"</summary><P/>

* Scroll down a bit
* Choose the down-triangle on the right side of the `Merge pull request` big green button
* From the drop-down options, select `Squash and merge`
* Now click the big green button that says `Squash and Merge`
* Edit the description as you like ... this will become the commit message
* Click the big green button that says `Confirm squash and merge`

</details>
<details><summary>5. Create a NEW PR from this NEW branch</summary><P/>

You already know how to do this.  :smiley:

</details>

<details><summary>Picture showing where admins only enable `Squash and merge` for depot</summary><P/>

![Options when only Squash and Merge enabled](https://user-images.githubusercontent.com/146967/85749445-b762c880-b6bd-11ea-89d7-6b88595ffa55.png)

</details>

## Updating a PR

When you receive a feedback requesting modifications to your PR, you can easily do them in the branch used for PR.
Before making any local changes make sure you update your local copy by issuing `git checkout <pr_branch>` followed by `git pull`. This will make sure your local copy received any available updates from the PR. Once you make and commit changes just push them to your repository using `git push`. PR will be updated automatically.

If you encounter conflicts you need to resolve those locally (by pulling, modifying and committing) before pushing it upstream. **Never** force push your changes, it messes the commit history and prevents collaboration on PR.