## Introduction

What if you have all the hardware in place but are new to WLED. For instance you asked a tech person to make it for you or bought it completely ready or got this as a gift. How to find out how everything works? There is a lot of documentation available but in general they zoom in into the technical details and only partly addresses how to use it.

This page gives an overview for end users. Where possible linking to (parts of) existing content, like a section on a webpage or a part of a YouTube movie.

This is work in progress and also never finished, as new functionality and also new instructions will continue to emerge.

Feel free to add (for now, keep the structure of the topics unchanged).

## Steps
* Setting up Wifi
* Using the WLED app
* Create your first effect
* Saving effects using presets
* Using sliders
* Changing colors and palettes
* Multiple segments
* Share presets with others using API command
* Updating WLED
* ...
* Trouble shooting

## Setting up Wifi
* See [Home](.), step 3, 4 and 5

## Updating WLED
...
### Advanced: Saving and Restoring Configuration files
E.g. in case of factory reset or esphome install
* Just go to the [WLED-IP]/edit page and download the cfg.json and presets.json files (right click on file name)! To restore, you just delete the existing ones, upload the ones you downloaded for the backup and reboot (from [Issue 146](https://github.com/Aircoookie/WLED/issues/146))
## Troubleshooting
See also [FAQ](./FAQ)
### All WLED settings gone
For instance due to crash, factory reset or update
1. See [Setting up Wifi](#setting-up-wifi)
1. Change server description in Settings / User Interface
1. Configuring a Led strip ...
