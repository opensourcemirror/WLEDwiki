# Important: https://kno.wled.ge replaced this wiki as the primary source of user documentation!
**This wiki is now deprecated and read-only, please instead consider making a pull request at kno.WLED.ge to improve the WLED documentation. This is easy and only requires your GitHub account and using the pen in the top right corner of the page you'd like to edit. Thank you!** This wiki will be gradually dismantled.

Please see https://kno.wled.ge/basics/getting-started/ for this information.

## This wiki is still in use for development-related information
* [Code execution speed considerations for developers](https://github.com/Aircoookie/WLED/issues/4206)
* [How to properly submit a PR](https://github.com/Aircoookie/WLED/wiki/How-to-properly-submit-a-PR)
 