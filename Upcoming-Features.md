These are currently on my list of what COULD be implemented, there is no guarantee if and when they will be available!

- Full segment support via web UI
- Presets can have names and save presets
- Create named playlists of presets
- Custom color palettes
- Settings are stored in SPIFFS
- Captive portal
- New unified UI
- Settings pages overhaul
- Rework of WiFi connection logic (reconnect after WiFi or power outage)