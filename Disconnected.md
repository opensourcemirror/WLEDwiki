## You are no longer connected to WLED-AP!

Your device has disconnected from the embedded WiFi Access Point of your WLED light.
Please try manually connecting to the network again!