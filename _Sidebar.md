### Basics	
- [Binary installation](https://github.com/Aircoookie/WLED/wiki/Install-WLED-binary)
- [Compile WLED](https://github.com/Aircoookie/WLED/wiki/Compiling-WLED)  	
- [FAQ](https://github.com/Aircoookie/WLED/wiki/FAQ)   	
- [Quick Start](https://github.com/Aircoookie/WLED/wiki)  	
- [Settings](https://github.com/Aircoookie/WLED/wiki/Settings)  	
- [Tutorials & Resources](https://github.com/Aircoookie/WLED/wiki/Learning-the-ropes)	

### Features	
- [List of effects and palettes](https://github.com/Aircoookie/WLED/wiki/List-of-effects-and-palettes)   	
- [Macros & Button](https://github.com/Aircoookie/WLED/wiki/Macros)   	
- [Multi strip](https://github.com/Aircoookie/WLED/wiki/Multi-strip)
- [Presets](https://github.com/Aircoookie/WLED/wiki/Presets)   	
- [Segments](https://github.com/Aircoookie/WLED/wiki/Segments)   	
- [Webserver sitemap](https://github.com/Aircoookie/WLED/wiki/Subpages)   	
- [Control a relay](https://github.com/Aircoookie/WLED/wiki/Control-a-relay-with-WLED)	

### Interfaces	
- [Blynk](https://github.com/Aircoookie/WLED/wiki/Blynk)   	
- [DMX Output](https://github.com/Aircoookie/WLED/wiki/DMX-Output)   	
- [E1.31 (DMX) / Art-Net](https://github.com/Aircoookie/WLED/wiki/E1.31-DMX)   	
- [UDP Realtime / tpm2.net](https://github.com/Aircoookie/WLED/wiki/UDP-Realtime-Control)   	
- [HTTP Request API](https://github.com/Aircoookie/WLED/wiki/HTTP-request-API)   	
- [Infrared](https://github.com/Aircoookie/WLED/wiki/Infrared-Control)   	
- [JSON API](https://github.com/Aircoookie/WLED/wiki/JSON-API)   	
- [MQTT](https://github.com/Aircoookie/WLED/wiki/MQTT)   	
- [Philips hue sync](https://github.com/Aircoookie/WLED/wiki/Philips-hue-sync)   
- [WebSocket](https://github.com/Aircoookie/WLED/wiki/Websocket)
- [WLED UDP sync](https://github.com/Aircoookie/WLED/wiki/Sync-WLED-devices-(UDP-Notifier))   	

### Advanced	
- [Compatible Hardware](https://github.com/Aircoookie/WLED/wiki/Compatible-hardware)   	
- [Compatible Software](https://github.com/Aircoookie/WLED/wiki/Compatible-software)   	
- [Home Automation configs](https://github.com/Aircoookie/WLED/wiki/Home-Automation-systems)  	
- [How to Compile WLED .bin file](https://github.com/Aircoookie/WLED/wiki/How-To-Compile-WLED-.bin-File)  	
- [How to add custom features](https://github.com/Aircoookie/WLED/wiki/Add-own-functionality)   	
- [Mapping](https://github.com/Aircoookie/WLED/wiki/Mapping)
- [Remote Access and IFTTT](https://github.com/Aircoookie/WLED/wiki/Remote-Access-and-IFTTT)   	
- [Security](https://github.com/Aircoookie/WLED/wiki/Security)
- [Wiring pro-tips](https://github.com/Aircoookie/WLED/wiki/Wiring-Pro-tips)
- [How to properly submit a PR](https://github.com/Aircoookie/WLED/wiki/How-to-properly-submit-a-PR)

### Links	
- [Youtube Channel](https://www.youtube.com/channel/UCHS8O5XQiLAxC_1XX4o4dDQ)   	
- [Android app](https://play.google.com/store/apps/details?id=com.aircoookie.WLED)   	
- [iOS app](https://apps.apple.com/us/app/wled/id1475695033)   	
- [Binaries and releases](https://github.com/Aircoookie/WLED/releases)   	
- [Contributors and credits](https://github.com/Aircoookie/WLED/wiki/Contributors-and-credits)   