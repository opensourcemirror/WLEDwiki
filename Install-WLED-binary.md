Please see https://kno.wled.ge/basics/install-binary/ for this information.

This wiki is now deprecated. Please consider contributing to kno.wled.ge by making improvements as a pull request. Thank you!
