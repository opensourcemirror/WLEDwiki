Please see https://kno.wled.ge/interfaces/philips-hue/ for this information.

This wiki is now deprecated. Please consider contributing to kno.wled.ge by making improvements as a pull request. Thank you!